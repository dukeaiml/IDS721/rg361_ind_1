+++
title = "Twitter Offense Classification"
date = 2023-12-01
[taxonomies]
categories=["Natural Language Processing"]
tags=["Python", "Machine Learning", "NLP"]
+++


Link to Github Repository: [Link](https://github.com/nogibjj/NLP_Final_Twitter-Offense-Classification)

## Project Overview
Online conversations on social media platforms like Twitter (Now X) allow people to freely express ideas. However, some comments involve offensive, toxic, or abusive language which negatively impacts users. Detecting and filtering out such content can enable healthier discussions. But manual moderation does not scale while automated solutions struggle with limited data.
This project develops generative and discriminative machine learning models for enhanced offensive tweet classification performance. A Naive Bayes model will generate synthetic offensive tweet data to augment limited real examples. Meanwhile, Long Short-Term Memory (LSTM) neural networks will provide discriminative power. Comparative assessment on real and synthetic test data will evaluate these approaches and advance automated moderation capabilities through robust models and data generation.
