+++
title = "Financial Stock Analysis"
date = 2023-12-01
summary="Testing the summary"
[taxonomies]
categories=["Data Engineering"]
tags=["Python", "PySpark", "Databricks", "Azure", "Flask", "Docker"]
+++

Link to Github Repository: [Link](https://github.com/nogibjj/Final_Project_Stock_Analysis) | 
Link to Application: [Link](https://definalproj.azurewebsites.net/) | 
Link to Video description of Project: [Link](https://www.youtube.com/watch?v=hJKhQFhQiTI)

## Project Overview
This Web app for this project is built using Flask which can be run locally and is hosted on Azure for public access. The project makes use of Delta Tables from Databricks, a docker image hosted on DockerHub and utilizes Azure Web App for deployment. Further, the project uses various APIs to interact with external websites for getting data to make predictions.

