# Static Portfolio Site Using Zola and Vercel

[![pipeline status](https://gitlab.com/dukeaiml/IDS721/rg361_ind_1/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/rg361_ind_1/-/commits/main)

## Overview
In this project a static site is generated using [Zola](https://www.getzola.org/) a Static Site Generator (SSG) built on Rust. The base site is further enhanced by using ``HTML`` and ``CSS`` and finally deployed on [Vercel](https://vercel.com/). The site is intended to be used as a portfolio site to showcase information such as contact details, projects etc.

The site is hosted on Vercel and is automatically updated and deployed whenever any changes are made in this repository. 
 
The site can be accessed by using the following [Link](https://ind-1-git-main-revanths-projects-78f58be3.vercel.app/)

Please find the project Overview Video [here](https://youtu.be/4XJjSmRPOzQ)

## Site Content

Currently, the site cotains the following pages:

1. **Home**: This page is the landing page (index page) of the site. It contains a brief introduction about me and links to my various profiles and contact details.

![Home Page](./resources/image_index.png)

2. **Projects**: This page contains a list of projects that I have worked on. Each project is displayed as a card containing the project name, and icons to indicate the expected reading time and number of words. On clicking on the project card, the user is taken to the project page which contains a detailed information of the project.

![Projects Page](./resources/image_projects.png)

3. **Categories**: This page contains a list of categories that the projects are grouped into. On clicking on a category, the user is taken to a page containing a list of projects that belong to that category.

![Categories Page](./resources/image_categories.png)

4. **Tags**: This page contains a list of tags that the projects are grouped into. On clicking on a tag, the user is taken to a page containing a list of projects that belong to that tag.

![Tags Page](./resources/image_tags.png)

5. **Project Pages**: Each project has a dedicated page containing detailed information about the project. The page contains the categories and tags under which the project falls under, relevant links and a detailed description of the project.

![Project Page](./resources/image_project_page.png)


## Site Development Process

1. **Installation of Zola**: The first step was to install Zola. The installation instructions can be found [here](https://www.getzola.org/documentation/getting-started/installation/).

2. **Creation of Repository**: A new repository was created on Gitlab to host the site and it was made into a zola file structure using the command `zola init <project_name>` command.

3. **Base Theme**: A base theme was chosen from the [Zola themes](https://www.getzola.org/themes/) site and the theme was downloaded to the ``themes`` folder. The theme name and the required basic configuration was added to the `config.toml` file.

4. **Modification and Content Creation**: Changes were made to the base template as required using ``HTML``, ``CSS`` and by chnaging the folder structure. content of the site was created using ``markdown`` files.

5. **Testing**: The site was tested locally using the command `zola serve` and the site was accessed at `http://127.0.0.1:1111` in the local machine.
![Local Site](./resources/image_serve.png)

6. **Deployment**: The site was deployed on Vercel by linking the Gitlab repository to Vercel. The site is automatically updated and deployed whenever any changes are made in this repository.

## Auto-Deployment on Changes
Whenever a change is made in the repository, the site is automatically updated and deployed on Gitlab pages and Vercel.

Gitlab Pipeline: (also Triggers the Vercel Deployment)  
![Gitlab Pipeline](./resources/image_gitlab_pipeline.png)

Vercel Pipeline:  
![Vercel Deployment](./resources/image_vercel_pipeline.png)

## Future Work
Some of the planned future work for the site is:
1. Add more projects and content to the site as and when they are completed.
2. Adding a search bar funtionality to the site.
3. Adding a dynamic backgroud to the site.
